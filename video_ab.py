#!/usr/bin/env python
#
# The MIT License (MIT)
#
# Copyright (c) 2016 Elliott Karpilovsky
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Script to help with A/B testing of video codecs. Given a file, it encodes it
# at various quality settings. Note that the output A/B videos are stored as
# lossless (to prevent additional artifacts from marring the test) and are
# large in size. A stats.txt file is also saved, with information on how the
# videos were generated (and their bitrates).

import argparse, atexit, decimal, os, platform, re, shlex, shutil, subprocess, sys, tempfile

STATS_FILENAME = 'stats.txt'

def quality_basename(q):
    """Returns the file basename of the AB video at quality q."""
    return 'quality_%s.mkv' % q


class FFMPEG(object):
    """This class contains functionality regarding FFMPEG commands. Normally,
    it would be a module in its own separate file. However, since I want this
    script to be stand-alone, it is represented here."""

    @staticmethod
    def read_length_sec(metadata):
        """Returns the length of the video, in seconds, from the FFMPEG metadata."""
        duration_re = re.compile(r'^  Duration: (\d\d):(\d\d):(\d\d)\.\d\d,', 
                                 re.MULTILINE)
        matches = duration_re.findall(metadata)
        if len(matches) != 1:
            raise ValueError('Expected single duration line. Matches: %s' % 
                             matches)
        match = matches[0]
        hours = int(match[0])
        minutes = int(match[1])
        seconds = int(match[2])
        return hours * 3600 + minutes * 60 + seconds

    @staticmethod
    def run(cmd):
        """Runs the command. Raises an exception if non-zero return status is 
        found. Otherwise returns (stdout, stderr)."""
        p = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
            stdin=subprocess.PIPE, close_fds=True)
        stdout, stderr = p.communicate()
        if p.returncode != 0:
            raise FFMPEG.CmdError(cmd, p.returncode, stdout, stderr)
        return stdout, stderr

    @staticmethod
    def read_metadata(ffmpeg_bin, fname):
        """Returns metadata about the file, returning the output from FFMPEG."""
        stderr = None
        cmd = [ffmpeg_bin, '-i', fname]
        try:
            stdout, stderr = FFMPEG.run(cmd)
            raise RuntimeError(
                "Reading metadata with FFMPEG should return "
                "status code of 1, but 0 was received. "
                "Information:\n{}".format(
                    FFMPEG.CmdError(cmd, p.returncode, stdout, stderr)))
        except FFMPEG.CmdError as e:
            if e.returncode != 1:
                raise RuntimeError(
                    "Reading file length with FFMPEG should return "
                    "status code of 1. Information:\n{}".format(str(e)))
            stderr = e.stderr
        return stderr.decode('utf-8')

    @staticmethod
    def logfile_prefix():
        """Returns a logfile prefix for FFMPEG that is unique on each invocation. 
        It is automatically deleted on program exit."""
        d = tempfile.mkdtemp(prefix='ffmpeg_')
        atexit.register(shutil.rmtree, d)
        return os.path.join(d, 'logfile')

    @staticmethod
    def ignore_output():
        """A destination for the output of ffmpeg that goes into the void."""
        if platform.system() == 'Windows':
            return 'NUL'
        elif platform.system() in ('Linux', 'Darwin'):
            return '/dev/null'
        else:
            raise RuntimeError(
                'Unsupported platform (must be Linux, MacOS X, or Windows): {}'.format(
                    platform.system()))

    @staticmethod
    def shell_cmd(cmd):
        """Convert a list of command/args into a single shell command."""
        return ' '.join(shlex.quote(x) for x in cmd)
    
    class CmdError(RuntimeError):
        """Error raised when a command fails to execute."""

        def __init__(self, cmd, returncode, stdout, stderr):
            readable_cmd = FFMPEG.shell_cmd(cmd)
            super(FFMPEG.CmdError, self).__init__(
                'Command: {}\n'
                'Return code: {}\n'
                'STDOUT: {}\n'
                'STDERR: {}\n'.format(readable_cmd, returncode,
                                      stdout.decode('utf-8'),
                                      stderr.decode('utf-8')))
            self.cmd = cmd
            self.returncode = returncode
            self.stdout = stdout
            self.stderr = stderr


def qscale_args(codec_args, ifname, ofname, qscale, force):
    """Theora/MPEG2/Xvid all use qscale; abstract the command line
    generation."""
    cmd = [
        '-i', ifname,
        '-c:v']
    cmd.extend(codec_args)
    cmd.extend(['-q:v', str(qscale)])
    if force:
        cmd.append('-y')
    cmd.append(ofname)
    return [cmd]


def input_output_metavars(ifname, ofname):
    """Maps the input and output filenames to the metavariable names INPUT_FILENAME
    and OUTPUT_FILENAME."""
    return {ifname: 'INPUT_FILENAME', ofname: 'OUTPUT_FILENAME'}


class Theora(object):
    
    @staticmethod
    def name():
        return 'theora'

    @staticmethod
    def commands_metavars(ifname, ofname, qscale, force=False):
        """Returns two elements. The first element is the 1-pass theora encoding args, 
        as [[arg1, arg2, ...]]. The second element is the mapping of filenames to
        metavariables."""
        return (qscale_args(['libtheora'], ifname, ofname, qscale, force),
                input_output_metavars(ifname, ofname))

    @staticmethod
    def quality_func(q):
        """Theora takes a quality value between 0 and 10."""
        return map_between(q, 0, 10)


class Xvid(object):

    @staticmethod
    def name():
        return 'xvid'

    @staticmethod
    def commands_metavars(ifname, ofname, qscale, force=False):
        """Returns two elements. The first element is the 1-pass xvid args, 
        as [[arg1, arg2, ...]]. The second element is the mapping of filenames to
        metavariables."""
        return (qscale_args(['mpeg4', '-vtag', 'xvid'], ifname, ofname, qscale, force),
                input_output_metavars(ifname, ofname))

    @staticmethod
    def quality_func(q):
        """Xvid takes a quality value between 1 and 31."""
        return map_between((1 - q), 1, 31)



class Mpeg2(object):

    @staticmethod
    def name():
        return 'mpeg2'

    @staticmethod
    def commands_metavars(ifname, ofname, qscale, force=False):
        """Returns two elements. The first element is the 1-pass mpeg2 args,
        as [[arg1, arg2, ...]]. The second element is the mapping of filenames to
        metavariables."""
        return (qscale_args(['mpeg2video'], ifname, ofname, qscale, force),
                input_output_metavars(ifname, ofname))

    @staticmethod
    def quality_func(q):
        """Mpeg2 takes a quality value between 1 and 31."""
        return map_between((1 - q), 1, 31)

    
class VP8(object):

    @staticmethod
    def name():
        return 'vp8'

    @staticmethod
    def commands_metavars(ifname, ofname, crf, force=False):
        """Returns two elements. The first element is the command line arguments to 
        pass to FFMPEG for the first pass and second pass of VP8 encoding. Note that
        the ffmpeg binary is omitted from the command. The second element is the
        mapping of filenames to metavariables."""
        log_dir = FFMPEG.logfile_prefix()
        cmd1 = [
            '-i', ifname,
            '-c:v', 'libvpx',
            '-pass', '1',
            # Note that quality=good and cpu-used=0 produces nearly
            # identical quality to using quality=best, but is much faster.
            '-quality', 'good',
            '-cpu-used', '0',
            '-crf', str(crf),
            # This is the upper bound on bitrate; set to 0 to indicate
            # unbounded.
            '-b:v', '0',
            '-threads', '8',
            '-passlogfile', log_dir,
            '-an', '-f', 'webm',
            '-y', FFMPEG.ignore_output()]
        cmd2 = [
            '-i', ifname,
        '-c:v', 'libvpx',
            '-pass', '2',
            '-quality', 'good',
            '-cpu-used', '0',
            '-crf', str(crf),
            '-b:v', '0',
            '-threads', '8',
            '-passlogfile', log_dir]
        if force:
            cmd2.append('-y')
        cmd2.append(ofname)
        mapping = input_output_metavars(ifname, ofname)
        mapping[log_dir] = 'LOG_FILE'
        return ([cmd1, cmd2], mapping)

    @staticmethod
    def quality_func(q):
        """Converts a quality rating to a CRF value. CRF for VP8 is between 4 and 63,
        with lower values corresponding to greater quality."""
        return map_between((1 - q), 4, 63)


class VP9(object):

    @staticmethod
    def name():
        return 'vp9'

    @staticmethod
    def commands_metavars(ifname, ofname, crf, force=False):
        """Returns two elements. The first is the command line arguments to pass
        to FFMPEG for the first and second pass of VP9 encoding. Note that the 
        ffmpeg binary is omitted from the command. The second is the mapping of
        filenames to metavariables."""
        log_dir = FFMPEG.logfile_prefix()
        cmd1 = [
            '-i', ifname,
            '-c:v',
            'libvpx-vp9',
            '-pass', '1',
            '-b:v', '0',
            '-crf', str(crf),
            '-threads', '8',
            '-speed', '4',
            '-tile-columns', '6',
            '-frame-parallel', '1',
            '-passlogfile', log_dir,
            '-an', '-f', 'webm',
            '-y', FFMPEG.ignore_output()]
        cmd2 = [
            '-i', ifname,
            '-c:v',
            'libvpx-vp9',
            '-pass', '2',
            '-b:v', '0',
            '-crf', str(crf),
            '-threads', '8',
            '-speed', '1',
            '-tile-columns', '6',
            '-frame-parallel', '1',
            '-auto-alt-ref', '1',
            '-lag-in-frames', '25',
            '-passlogfile', log_dir]
        if force:
            cmd2.append('-y')
        cmd2.append(ofname)
        mapping = input_output_metavars(ifname, ofname)
        mapping[log_dir] = 'LOG_FILE'
        return ([cmd1, cmd2], mapping)

    @staticmethod
    def quality_func(q):
        """Converts a quality rating to a CRF value. CRF for VP9 is between 1 and 63,
        with lower values corresponding to greater quality."""
        return map_between((1 - q), 1, 63)
    

def x26x_args(codec, ifname, ofname, crf, force):
    """x264 and x265 share the same arguments; abstract the command line
    generation."""
    cmd = [
        '-i', ifname,
        '-c:v', codec]
    if crf == 0:
        # qp is recommended over crf if the value is 0. veryslow is also
        # recommended as the preset.
        cmd.extend(['-qp', '0',
                    '-preset', 'veryslow'])
    else:
        cmd.extend(['-crf', str(crf),
                    '-preset', 'slower'])  # Second slowest option.
    if force:
        cmd.append('-y')
    cmd.append(ofname)
    return [cmd]


class X264(object):

    @staticmethod
    def name():
        return 'x264'

    @staticmethod
    def commands_metavars(ifname, ofname, crf, force=False):
        """Returns two elements. The first is the 1-pass x264 args, 
        as [[arg1, arg2, ...]]. The second is the mapping from filenames
        to metavariables."""
        return (x26x_args('libx264', ifname, ofname, crf, force),
                input_output_metavars(ifname, ofname))

    @staticmethod
    def quality_func(q):
        """x264 takes a quality value between 0 and 51, where lower is
        better quality."""
        return map_between((1 - q), 0, 51)


class X265(object):

    @staticmethod
    def name():
        return 'x265'

    @staticmethod
    def commands_metavars(ifname, ofname, crf, force=False):
        """Returns two elements. The first is the 1-pass x265 args, 
        as [[arg1, arg2, ...]]. The second is the mapping from filenames
        to metavariables."""
        return (x26x_args('libx265', ifname, ofname, crf, force),
                input_output_metavars(ifname, ofname))

    @staticmethod
    def quality_func(q):
        """x265 takes a quality value between 0 and 51, where lower is
        better quality."""
        return map_between((1 - q), 0, 51)


def quality_range(min_q, max_q, inc_q):
    """Iterates through the qualities, inclusive of both ends."""
    curr_q = min_q
    while curr_q <= max_q:
        yield curr_q
        curr_q += inc_q

        
def normalize(q_range, q_to_f):
    """q_to_f a function that maps qualities to codec specific values.
    If a quality in q_range will generate the same codec values as a
    previous call, it is skipped."""
    values = set()
    for q in q_range:
        v = q_to_f(q)
        if v in values:
            continue
        values.add(v)
        yield q

        
def map_between(q, start, end):
    """Linearly maps quality to an integer between start and end."""
    delta = end - start
    offset = (q * delta).to_integral_value(decimal.ROUND_HALF_EVEN)
    return int(start + offset)


CODECS = (Theora, VP8, VP9, X264, X265, Xvid, Mpeg2)


MODES = {
    # Puts the two videos side-by-side, with the transcoded video on the right.
    'side-right': (
        '-filter_complex', '[0:v]setpts=PTS-STARTPTS, pad=iw*2:ih[bg]; ' +
        '[1:v]setpts=PTS-STARTPTS[fg]; [bg][fg]overlay=w'),
    # Puts the two videos side-by-side, with the transcoded video on the left.
    'side-left': (
        '-filter_complex', '[1:v]setpts=PTS-STARTPTS, pad=iw*2:ih[bg]; ' +
        '[0:v]setpts=PTS-STARTPTS[fg]; [bg][fg]overlay=w'),
    # Bottom-half of the video is transcoded, top-half is original.
    'bottom': (
        '-filter_complex', '[0:v]setpts=PTS-STARTPTS, crop=iw:ih/2:0:0[top]; ' +
        '[1:v]setpts=PTS-STARTPTS, crop=iw:ih/2:0:oh[bottom]; [top][bottom]vstack'),
    # Top-half of the video is transcoded, bottom-half is original.
    'top': (
        '-filter_complex', '[1:v]setpts=PTS-STARTPTS, crop=iw:ih/2:0:0[top]; ' +
        '[0:v]setpts=PTS-STARTPTS, crop=iw:ih/2:0:oh[bottom]; [top][bottom]vstack'),
    # Right-half of the video is transcoded, left-half is original.
    'right': (
        '-filter_complex','[0:v]setpts=PTS-STARTPTS, crop=iw/2:ih:0:0[left]; ' +
        '[1:v]setpts=PTS-STARTPTS, crop=iw/2:ih:ow:0[right]; [left][right]hstack'),
    # Left-half of the video is transcoded, right-half is original.
    'left': (
        '-filter_complex','[1:v]setpts=PTS-STARTPTS, crop=iw/2:ih:0:0[left]; ' +
        '[0:v]setpts=PTS-STARTPTS, crop=iw/2:ih:ow:0[right]; [left][right]hstack')
}


# Parsers / validators for the command line arguments and flags.
class CodecAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        for c in CODECS:
            if values.lower() == c.name().lower():
                setattr(namespace, self.dest, c)
                return
        parser.error("codec must be one of {}".format(
            ', '.join(sorted(x.name() for x in CODECS))))

class OutputDirAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if not os.path.isdir(values):
            parser.error("{} is not a directory".format(values))
        setattr(namespace, self.dest, values)

class QualityAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if not (0 <= values <= 1):
            parser.error("quality bounds must be between 0 and 1 (inclusively)")
        setattr(namespace, self.dest, values)

class IncQualityAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if not (0 < values <= 1):
            parser.error("inc-quality must be between 0 (exclusive) and 1 (inclusive)")
        setattr(namespace, self.dest, values)

class ModeAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if values not in MODES:
            parser.error("mode must be one of {}".format(
                ', '.join(sorted(MODES))))
        setattr(namespace, self.dest, MODES[values])


PARSER = argparse.ArgumentParser(
    description="Generates videos for A/B testing.")
PARSER.add_argument('input', help='input file to A/B test')
PARSER.add_argument('output_dir', action=OutputDirAction,
                    help="directory to save the A/B videos and stats.txt file")
PARSER.add_argument('codec', action=CodecAction,
                    help='one of: %s' % ', '.join(sorted(x.name() for x in CODECS)))
PARSER.add_argument('-b', '--ffmpeg-bin', metavar='/path/to/ffmpeg', default='ffmpeg',
                    help='location of the FFMPEG binary')
PARSER.add_argument('--min-quality', metavar='MIN_Q', action=QualityAction, default="0.0",
                    type=decimal.Decimal, help='minimum quality of videos in A/B test')
PARSER.add_argument('--max-quality', metavar='MAX_Q', action=QualityAction, default="1.0",
                    type=decimal.Decimal, help='maximum quality of videos in A/B test')
PARSER.add_argument('--inc-quality', metavar='INC_Q', action=IncQualityAction, default="0.1",
                    type=decimal.Decimal, help='quality increment between A/B tests')
PARSER.add_argument('-m', '--mode', default=MODES["side-right"], action=ModeAction,
                    help=('how the A/B videos should be composed: %s' %
                          ', '.join(sorted(MODES))))
PARSER.add_argument('-f', '--force', dest='force', action='store_true',
                    help='force overwriting of output files')


def print_exit(s, return_code):
    """Prints the message to stderr, then exits with the return code."""
    print(s, file=sys.stderr)
    sys.exit(return_code)
   

def user_friendly_bitrate(brate):
    """Converts a bitrate into a human readable string of bits/s."""
    return '%.2e bits/s' % brate


def save_stats(ffmpeg_bin, input_fname, output_dir, codec, orig_avg_bitrate,
               quality_bitrate, quality_cmds, force):
    """Saves statistics (and command lines used to generate A/B files) in
    the output directory, in a stats file."""
    stats = os.path.join(output_dir, STATS_FILENAME)
    if not force and os.path.exists(stats):
        print_exit('Already exists: ' + stats, os.EX_DATAERR)

    with open(stats, 'wb') as f:
        f.write('Filename: {}\n'.format(input_fname).encode())
        f.write('Video bitrate: {}\n'.format(
            user_friendly_bitrate(orig_avg_bitrate)).encode())
        f.write('Codec: {}'.format(codec.name()).encode())
        for quality in sorted(quality_bitrate):
            f.write('\n\nQuality: {}\n'.format(quality).encode())
            cmds = [[ffmpeg_bin] + c for c in quality_cmds[quality]]
            f.write('Command: {}\n'.format(
                '; '.join(FFMPEG.shell_cmd(c) for c in cmds))
                    .encode())
            f.write('Video bitrate: {}\n'.format(
                user_friendly_bitrate(quality_bitrate[quality])).encode())
            f.write(('Compression: %.2f%%' %
                     ((100.0 * quality_bitrate[quality]) / orig_avg_bitrate))
                    .encode())
        f.write('\n'.encode())


def calc_bitrate(ffmpeg_bin, fname):
    """Calculates the bitrate of the video file."""
    metadata = FFMPEG.read_metadata(ffmpeg_bin, fname)
    length_sec = FFMPEG.read_length_sec(metadata)
    return (8.0 * os.path.getsize(fname)) / length_sec

    
def split_video(ffmpeg_bin, ifile, video_fname, no_video_fname):
    """Splits the video into just the video stream, and all other streams."""
    cmd = [ffmpeg_bin, '-fflags', '+genpts', '-i', ifile, '-map', '0:v',
           '-c:v', 'copy', '-y', video_fname]
    FFMPEG.run(cmd)
    cmd = [ffmpeg_bin, '-fflags', '+genpts', '-i', ifile, '-map', '0',
           '-c', 'copy', '-vn', '-y', no_video_fname]
    FFMPEG.run(cmd)
        
    
def ab_encode(ffmpeg_bin, video_fname, no_video_fname, output_dir, codec, quality,
              quality_bitrate, quality_cmds, mode, force):
    """Generates a mkv file of the A/B test at the given quality. quality_bitrate is 
    updated with the bitrate for the given quality, and quality_cmds is updated to
    record the commands used to generate the video."""
    with tempfile.NamedTemporaryFile(prefix='video_ab_', suffix='.mkv') as f:
        cmds, mapping = codec.commands_metavars(
            video_fname, f.name, codec.quality_func(quality), True)
        for cmd in cmds:
            FFMPEG.run([ffmpeg_bin] + cmd)
        quality_cmds[quality] = replace_with_metavars(cmds, mapping)
        quality_bitrate[quality] = calc_bitrate(ffmpeg_bin, f.name)

        # FFMPEG only picks a single soundtrack/subtitle track when using
        # -filter_complex (which is used for the side-by-side/overlay mode).
        # The video is split, merged, and then re-combined with non-video parts
        # to ensure the final output has all the tracks of the original.
        with tempfile.NamedTemporaryFile(prefix='video_ab_', suffix='.mkv') as m:
            merge_ab(ffmpeg_bin, mode, video_fname, f.name, m.name)
            cmd = [ffmpeg_bin, '-i', m.name, '-i', no_video_fname, '-map', '0',
                   '-map', '1', '-c', 'copy']
            if force:
                cmd.append('-y')
            cmd.append(os.path.join(output_dir, quality_basename(quality)))
            FFMPEG.run(cmd)


def replace_with_metavars(cmds, mapping):
    """Replaces all instances of names in the mapping with the metavariables.
    E.g., the input filename /tmp/foasdg.mkv becomes INPUT_FILENAME."""
    result = []
    for cmd in cmds:
        result.append([mapping.get(c, c) for c in cmd])
    return result

    
def merge_ab(ffmpeg_bin, mode, fname1, fname2, output_fname):
    """Compose the two videos (according to the mode)."""
    cmd = [ffmpeg_bin, '-i', fname1, '-i', fname2]
    cmd.extend(mode)
    cmd.extend(('-c:v', 'huffyuv', '-y', output_fname))
    FFMPEG.run(cmd)


def check_output_empty(output_dir, quality_range):
    """Perform a quick check that there are not output files in the output
    directory (since the program can take a while before realizing this issue)."""
    filenames = [quality_basename(q) for q in quality_range]
    filenames.append(STATS_FILENAME)
    existing = []
    for f in filenames:
        fname = os.path.join(output_dir, f)
        if os.path.isfile(fname):
            existing.append(f)
    if existing:
        raise RuntimeError('Output already exists in {}: {}'.format(
            output_dir, ' '.join(existing)))

def run(ffmpeg_bin, input_fname, output_dir, codec, min_quality, max_quality,
        inc_quality, mode, force):
    """Generates the A/B videos from the input file, saving them in the output
    directory. If force is set, will overwrite existing files. The number of generated
    videos is controlled through the quality settings. Mode determines how the A/B
    videos will be composed."""
    q_range = list(quality_range(min_quality, max_quality, inc_quality))
    if not force:
        check_output_empty(output_dir, q_range)
    # Split the video into just the video component and everything else.
    with tempfile.NamedTemporaryFile(prefix='just_video_', suffix='.mkv') as video:
        with tempfile.NamedTemporaryFile(prefix='no_video_', suffix='.mkv') as no_video:
            split_video(ffmpeg_bin, input_fname, video.name, no_video.name)

            orig_avg_bitrate = calc_bitrate(ffmpeg_bin, video.name)
            quality_bitrate = {}
            quality_cmds = {}
            for quality in normalize(q_range, codec.quality_func):
                ab_encode(ffmpeg_bin, video.name, no_video.name, output_dir, codec,
                          quality, quality_bitrate, quality_cmds, mode, force)
    save_stats(ffmpeg_bin, input_fname, output_dir, codec, orig_avg_bitrate,
               quality_bitrate, quality_cmds, force)


if __name__ == '__main__':
    args = PARSER.parse_args()
    if args.min_quality > args.max_quality:
        print_exit("minimum quality cannot be greater than maximum", os.EX_USAGE)

    if not os.path.isfile(args.input):
        print_exit('input must be a file', os.EX_USAGE)

    try:
        run(args.ffmpeg_bin, args.input, args.output_dir, args.codec,
            args.min_quality, args.max_quality, args.inc_quality, args.mode,
            args.force)
    except Exception as e:
        print_exit('\n' + str(e) + '\nError while processing.', os.EX_SOFTWARE)

