#!/usr/bin/env python
#
# The MIT License (MIT)
#
# Copyright (c) 2016 Elliott Karpilovsky
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Tests for the video slicer code.

import os, shutil, tempfile, unittest
import ffmpeg_test, video_slicer

# Path to the FFMPEG binary.
FFMPEG_BIN = 'ffmpeg'

class VideoSlicerTest(unittest.TestCase):

    # Scratch directory to use for the tests.
    TEST_DIR = None
    
    @classmethod
    def setUpClass(cls):
        cls.TEST_DIR = tempfile.mkdtemp()
        print('Generating test video - this may take some time.')
        ffmpeg_test.gen_fading_video(FFMPEG_BIN, cls.input_name())

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls.TEST_DIR)

    def tearDown(self):
        if os.path.isfile(self.output_name()):
            os.remove(self.output_name())
            
    @classmethod
    def input_name(cls):
        return os.path.join(cls.TEST_DIR, 'input.mkv')

    @classmethod
    def output_name(cls):
        return os.path.join(cls.TEST_DIR, 'output.mkv')

    def runVideoSlicer(self, seconds, offset, ffmpeg_args, force):
        """Runs the video slicer on input_name() and output_name() with FFMPEG_BIN."""
        video_slicer.run(FFMPEG_BIN, self.input_name(), self.output_name(),
                         seconds, offset, ffmpeg_args, force)

    def testAllStreamsCopied(self):
        """Verifies that all streams are copied if the FFMPEG args are set correctly."""
        self.runVideoSlicer(None, None, '-map 0 -c copy', False)
        streams = ffmpeg_test.read_streams(FFMPEG_BIN, self.output_name()) 
        self.assertEqual([ffmpeg_test.VIDEO,
                          ffmpeg_test.AUDIO, ffmpeg_test.AUDIO,
                          ffmpeg_test.SUBTITLE, ffmpeg_test.SUBTITLE], streams)

    def testJustVideoCopied(self):
        """Verifies that just video is copied if FFMPEG args are set correctly."""
        self.runVideoSlicer(None, None, '-map 0:v -c:v copy', False)
        streams = ffmpeg_test.read_streams(FFMPEG_BIN, self.output_name()) 
        self.assertEqual([ffmpeg_test.VIDEO], streams)

    def testNoForce(self):
        """If the file exists and force is not set, an error is raised."""
        with open(self.output_name(), 'w') as f:
            f.write('')
        self.assertRaises(video_slicer.FFMPEG.CmdError,
                          self.runVideoSlicer, None, None, None, False)

    def testForce(self):
        """If force is set, the file is overwritten."""
        with open(self.output_name(), 'w') as f:
            f.write('')
        self.runVideoSlicer(None, None, None, True)
        
    def testOffset(self):
        """Test setting the offset."""
        metadata = video_slicer.FFMPEG.read_metadata(FFMPEG_BIN,
                                                     self.input_name())
        length_sec = video_slicer.FFMPEG.read_length_sec(metadata)
        for i in range(length_sec):
            self.runVideoSlicer(None, video_slicer.to_hhmmss(i), '-c copy', True)
            m = video_slicer.FFMPEG.read_metadata(FFMPEG_BIN,
                                                  self.output_name())
            out_length_sec = video_slicer.FFMPEG.read_length_sec(m)
            self.assertEqual(length_sec - i, out_length_sec)

    def testInfeasibleOffset(self):
        """If an offset is requested that is beyond the video length,
        an error should be raised."""
        metadata = video_slicer.FFMPEG.read_metadata(FFMPEG_BIN,
                                                     self.input_name())
        length_sec = video_slicer.FFMPEG.read_length_sec(metadata)
        self.assertRaises(ValueError, self.runVideoSlicer,
                          None, video_slicer.to_hhmmss(length_sec),
                          '-c copy', True)
        
    def testInfeasibleOffsetWindow(self):
        """If an offset is requested, but the window size is larger than the
        remaining video, an error should be raised."""
        metadata = video_slicer.FFMPEG.read_metadata(FFMPEG_BIN,
                                                     self.input_name())
        length_sec = video_slicer.FFMPEG.read_length_sec(metadata)
        # This should succeed.
        self.runVideoSlicer(length_sec, '00:00:00', '-c copy', True)
        # This should fail.
        self.assertRaises(ValueError, self.runVideoSlicer,
                          length_sec, '00:00:01', '-c copy', True)
        
    def testInfeasibleRandomOffset(self):
        """If a random offset is requested, but the window size is larger than
        the length of the video, an error should be raised."""
        metadata = video_slicer.FFMPEG.read_metadata(FFMPEG_BIN,
                                                     self.input_name())
        length_sec = video_slicer.FFMPEG.read_length_sec(metadata)
        # This should succeed.
        self.runVideoSlicer(length_sec, 'random', '-c copy', True)
        # This should fail.
        self.assertRaises(ValueError, self.runVideoSlicer,
                          length_sec + 1, 'random', '-c copy', True)

    def testRandomOffset(self):
        """Generate random offsets until all offsets are covered."""
        metadata = video_slicer.FFMPEG.read_metadata(FFMPEG_BIN,
                                                     self.input_name())
        length_sec = video_slicer.FFMPEG.read_length_sec(metadata)
        seen_offsets = set()
        while len(seen_offsets) < length_sec:
            self.runVideoSlicer(None, 'random', '-c copy', True)
            m = video_slicer.FFMPEG.read_metadata(FFMPEG_BIN,
                                                  self.output_name())
            seen_offsets.add(length_sec - video_slicer.FFMPEG.read_length_sec(m))

    def testSeconds(self):
        """Test video length is affected by window size."""
        metadata = video_slicer.FFMPEG.read_metadata(FFMPEG_BIN,
                                                     self.input_name())
        length_sec = video_slicer.FFMPEG.read_length_sec(metadata)
        for i in range(1, length_sec + 1):
            self.runVideoSlicer(i, None, '-c copy', True)
            m = video_slicer.FFMPEG.read_metadata(FFMPEG_BIN,
                                                  self.output_name())
            self.assertEqual(i, video_slicer.FFMPEG.read_length_sec(m))
        
    
if __name__ == '__main__':
    unittest.main()
