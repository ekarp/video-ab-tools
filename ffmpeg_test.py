#!/usr/bin/env python
#
# The MIT License (MIT)
#
# Copyright (c) 2016 Elliott Karpilovsky
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Tests for FFMPEG static class inside video_ab and video_slicer. Also contains
# library functions for generating test videos:
#
#   gen_fading_video generates a video that fades from white to black. It has
#   two audio streams (at constant frequencies) and two subtitle tracks
#
#   gen_noise_video generates a video of static noise. It has a single audio
#   track of white noise and a single subtitle track with random letters
#
#   read_streams returns a list of keywords indicating the streams in the video

import argparse, math, os, random, re, shutil, struct, subprocess, tempfile, wave, unittest
from PIL import Image
import video_ab
import video_slicer

VIDEO = 'Video'
AUDIO = 'Audio'
SUBTITLE = 'Subtitle'

# Properties of the fading and noise video.
__WIDTH_PIX = 640
__HEIGHT_PIX = 480
__FPS = 30
__FADING_LENGTH_SEC = 10
__NOISE_LENGTH_SEC = 1


def gen_fading_video(ffmpeg_bin, fname):
    """Creates the fading video in the file path."""
    tmpdir = tempfile.mkdtemp()
    try:
        cmd = __create_video(
            [__write_fixed_video_stream(tmpdir, __FADING_LENGTH_SEC),
             __write_fixed_audio_stream(
                 tmpdir, hz=200, length_sec=__FADING_LENGTH_SEC),
             __write_fixed_audio_stream(
                 tmpdir, hz=800, length_sec=__FADING_LENGTH_SEC),
             __write_fixed_subtitle_stream(
                 tmpdir, "Subtitle stream 1", __FADING_LENGTH_SEC),
             __write_fixed_subtitle_stream(
                 tmpdir, "Subtitle stream 2", __FADING_LENGTH_SEC)],
            fname)
        video_slicer.FFMPEG.run([ffmpeg_bin] + cmd)
    finally:
        shutil.rmtree(tmpdir)


def gen_noise_video(ffmpeg_bin, fname):
    """Generates a video of noise in the file path."""
    tmpdir = tempfile.mkdtemp()
    try:
        cmd = __create_video(
            [__write_random_video_stream(tmpdir, __NOISE_LENGTH_SEC),
             __write_random_audio_stream(tmpdir, __NOISE_LENGTH_SEC),
             __write_random_subtitle_stream(tmpdir, __NOISE_LENGTH_SEC)],
            fname)
        video_slicer.FFMPEG.run([ffmpeg_bin] + cmd)
    finally:
        shutil.rmtree(tmpdir)


def read_streams(ffmpeg_bin, fname):
    """Returns a list of streams composing the video."""
    stream_re = re.compile(
        r'^\s+Stream #0:(?P<track>\d+)(\[[^]]+\])?: (?P<type>\S+): ',
        re.MULTILINE)
    allowed_streams = frozenset([VIDEO, AUDIO, SUBTITLE])
    result = []
    metadata = video_slicer.FFMPEG.read_metadata(ffmpeg_bin, fname)
    expected_num = 0
    for match in stream_re.finditer(metadata):
        track = int(match.groupdict()['track'])
        if track != expected_num:
            raise ValueError('Expected stream 0:{}, but found 0:{}'.format(
                expected_num, track))
        expected_num += 1
        t = match.groupdict()['type']
        if t not in allowed_streams:
            raise ValueError('Unknown stream type: {}'.format(t))
        result.append(t)
    return result


# Path to the FFMPEG binary.
FFMPEG_BIN = 'ffmpeg'

class FfmpegTest(unittest.TestCase):

    # Scratch directory for storing the test video.
    INPUT_DIR = None

    @classmethod
    def setUpClass(cls):
        cls.INPUT_DIR = tempfile.mkdtemp()
        print('\nGenerating test videos - this may take some time.')
        gen_fading_video(FFMPEG_BIN, cls.fading_name())
        gen_noise_video(FFMPEG_BIN, cls.noise_name())

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls.INPUT_DIR)

    @classmethod
    def fading_name(cls):
        return os.path.join(cls.INPUT_DIR, 'fading.mkv')

    @classmethod
    def noise_name(cls):
        return os.path.join(cls.INPUT_DIR, 'noise.mkv')

    def setUp(self):
        self.output_dir = tempfile.mkdtemp()
        
    def tearDown(self):
        shutil.rmtree(self.output_dir)

    def testSame(self):
        """Verifies that both video_ab and video_slicer have the exact same code
        for FFMPEG (and that the code is at least 3000 characters)."""
        ab_block = self.extract_ffmpeg_block(video_ab.__file__)
        slicer_block = self.extract_ffmpeg_block(video_slicer.__file__)
        self.assertEqual(ab_block, slicer_block)
        self.assertTrue(3000 <= len(ab_block))

    def extract_ffmpeg_block(self, fname):
        with open(fname) as f:
            text = f.read()
        regions = text.split("\nclass FFMPEG(object):\n")
        if len(regions) != 2:
            raise ValueError('Expected exactly one FFMPEG class (with no trailing '
                             'or leading spaces) in {}'.format(fname))
        post = regions[1]
        return re.split('\n(def|class) ', post)[0].strip()

    def testFfmpegReadLengthSec(self):
        # Noise video is 1 second long, fading video is 10 seconds.
        metadata = video_ab.FFMPEG.read_metadata(
            FFMPEG_BIN, self.noise_name())
        length_sec = video_ab.FFMPEG.read_length_sec(metadata)
        self.assertEqual(1, length_sec)

        metadata = video_ab.FFMPEG.read_metadata(
            FFMPEG_BIN, self.fading_name())
        length_sec = video_ab.FFMPEG.read_length_sec(metadata)
        self.assertEqual(10, length_sec)

    def testReadStreams(self):
        self.assertEqual(
            [VIDEO, AUDIO, AUDIO, SUBTITLE, SUBTITLE],
            read_streams(FFMPEG_BIN, os.path.join(self.output_dir, self.fading_name())))
        self.assertEqual(
            [VIDEO, AUDIO, SUBTITLE],
            read_streams(FFMPEG_BIN, os.path.join(self.output_dir, self.noise_name())))
        
    def testShellCmd(self):
        self.assertEqual("ls", video_ab.FFMPEG.shell_cmd(["ls"]))
        self.assertEqual("ls -l", video_ab.FFMPEG.shell_cmd(["ls", "-l"]))


class CmdErrorTest(unittest.TestCase):

    def testConstruct(self):
        e = video_ab.FFMPEG.CmdError(['bin', 'arg'], 1, 'stdout'.encode(),
                                     'stderr'.encode())
        self.assertTrue('bin' in str(e))
        self.assertTrue('arg' in str(e))
        self.assertTrue('1' in str(e))
        self.assertTrue('stdout' in str(e))
        self.assertTrue('stderr' in str(e))
        
        
def __create_video(sources, filename):
    """Returns the command to pass to FFMPEG to generate a video from the input
    sources. Note that the video source must appear first, is assumed to be 30 FPS, 
    and is encoded using huffyuv. Subtitles and audio are copied over without changes."""
    cmd = ['-framerate', '30', '-i', sources[0]]
    for s in sources[1:]:
        cmd.extend(['-i', s])
    for i in range(len(sources)):
        cmd.extend(['-map', str(i)])
    return cmd + ['-c:v', 'huffyuv', '-c:a', 'copy', '-c:s', 'copy', '-r', '30',
                  '-pix_fmt', 'yuv420p', '-y', filename]


# Code for creating either a fading video or a static noise video. These functions
# create a directory inside dirname and store PNGs in there. __FPS * length_sec PNGs
# are created. Width and height are __WIDTH_PIX and __HEIGHT_PIX. Returns a file
# pattern that can be used by ffmpeg to construct a video from the images.
def __write_fixed_video_stream(dirname, length_sec):
  return __write_video_stream(dirname, length_sec, __fading_frame)


def __write_random_video_stream(dirname, length_sec):
  return __write_video_stream(dirname, length_sec, __noise_frame)


def __write_video_stream(dirname, length_sec, frame_generator):
  src = tempfile.mkdtemp(dir=dirname)
  total_frames = length_sec * __FPS
  for i in range(total_frames):
    img = Image.new('RGB', (__WIDTH_PIX, __HEIGHT_PIX))
    img.putdata(frame_generator(i, total_frames, __WIDTH_PIX, __HEIGHT_PIX))
    img.save(os.path.join(src, str(i) + ".png"))
  return os.path.join(src, '%d.png')


def __fading_frame(i, total_frames, width, height):
    # (255, 255, 255) is white, and (0, 0, 0) is black.
    c = int((255.0 * (total_frames - i - 1)) / total_frames)
    return [(c, c, c)] * width * height


def __noise_frame(i, total_frames, width, height):
    r = []
    for _ in range(width * height):
        c = random.randint(0, 255)
        r.append((c, c, c))
    return r
  

# Code for creating either a frequency beep or static noise. Returns a file
# path to a wave file.
def __write_fixed_audio_stream(dirname, hz, length_sec):
    return __write_audio_stream(dirname, length_sec, __hz_generator(hz))


def __write_random_audio_stream(dirname, length_sec):
    return __write_audio_stream(dirname, length_sec, __noise_audio)


def __write_audio_stream(dirname, length_sec, frame_generator):
    frame_rate = 44100  # Number of samples per second.
    sample_width_bytes = 2  # 16-bit audio.
    num_channels = 2  # Left and right audio channels.
    f = tempfile.NamedTemporaryFile(dir=dirname, suffix=".wav", delete=False)
    with wave.open(f.name, 'wb') as output:
        output.setnchannels(num_channels)
        output.setsampwidth(sample_width_bytes)
        output.setframerate(frame_rate)
        output.setnframes(0)  # The stream is initially empty.
        output.setcomptype('NONE', 'no compression')
        for i in range(frame_rate * length_sec):
            frame = frame_generator(i, frame_rate)
            output.writeframes(frame)  # Left channel.
            output.writeframes(frame)  # Right channel.
    return f.name


def __hz_generator(hz):
    def generator(i, frame_rate):
        samples_per_freq_cycle = int(frame_rate / hz)
        max_val = 32767  # Maximum value for a signed 16-bit number.
        sample = max_val * math.sin(math.pi * 2 * (i % samples_per_freq_cycle) /
                                    samples_per_freq_cycle)
        return struct.pack('h', int(sample))
    return generator
        

def __noise_audio(i, frame_rate):
    val = random.randint(-32768, 32767)  # Min/max values for a signed 16-bit number.
    return struct.pack('h', val)


# Code for creating subtitle files.
def __write_fixed_subtitle_stream(dirname, text, length_sec):
    if '\n\n' in text:
        raise ValueError('text must contain no blank lines for subtitles: %s' % text)
    f = tempfile.NamedTemporaryFile(dir=dirname, suffix=".srt", delete=False)
    with open(f.name, 'w') as output:
        # Write out the same information every 500 milliseconds, so if someone switches
        # subtitle streams, it will change fairly soon. Note that VLC adds significant
        # delay when switching, and it can also cause flashing each time the subtitle
        # is re-rendered over the title of the video.
        milliseconds = length_sec * 1000
        start_ms = 0
        i = 1
        inc_ms = 500
        while start_ms < milliseconds:
            output.write('%d\n' % i)
            output.write('%s --> %s\n' % (__to_hour_min_sec_ms(start_ms),
                                          __to_hour_min_sec_ms(start_ms + inc_ms)))
            output.write(text + '\n\n')
            start_ms += inc_ms
            i += 1
    return f.name


def __to_hour_min_sec_ms(msec):
  """Converts the number of milliseconds into a HH:MM:SS,000 string."""
  millisecs = msec % 1000
  remainder = msec / 1000
  seconds = remainder % 60
  remainder = remainder / 60
  minutes = remainder % 60
  hours = remainder / 60
  if hours > 99:
    raise ValueError('maximum supported time of video is 99 hours, 59 minutes, 59 seconds, and ' +
                     '999 milliseconds; given %d milliseconds' % msec)
  return '%02d:%02d:%02d,%03d' % (hours, minutes, seconds, millisecs)
                     

def __write_random_subtitle_stream(dirname, length_sec):
    text = []
    for _ in range(100):
        text.append(chr(random.randint(32, 126)))  # Printable ASCII range.
    return __write_fixed_subtitle_stream(dirname, ''.join(text), length_sec)    


if __name__ == '__main__':
    unittest.main()


