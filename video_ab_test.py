#!/usr/bin/env python
#
# The MIT License (MIT)
#
# Copyright (c) 2016 Elliott Karpilovsky
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Tests for the video A/B tester.

import decimal, os, re, shutil, tempfile, time, unittest
import ffmpeg_test, video_ab

# Path to the FFMPEG binary.
FFMPEG_BIN = 'ffmpeg'

# Corresponds to a single block of text in the stats file about one quality.
QUALITY_RE = re.compile(
    r'\n\nQuality: (?P<quality>\S+)\nCommand: .*\nVideo bitrate: (?P<bitrate>\S+) bits/s\n')

class QualityRangeTest(unittest.TestCase):
    """Test the quality_range and normalize functions."""

    def testEmpty(self):
        self.assertEqual([], list(video_ab.quality_range(1, 0.9, 1)))

    def testBothEndsInclusive(self):
        self.assertEqual([1], list(video_ab.quality_range(1, 1, 1)))
        self.assertEqual([1], list(video_ab.quality_range(1, 1.1, 1)))
        self.assertEqual([1, 2], list(video_ab.quality_range(1, 2, 1)))

    def testIncrement(self):
        self.assertEqual(
            [1, 2, 3, 4, 5],
            list(video_ab.quality_range(1, 5, 1)))

    def testNormalizeQualities(self):
        """Mark all odd numbers as actually representing the even numbers above them; verify
        that the generated numbers are distinct, and the pre-transformed numbers are returned."""
        self.assertEqual(
            [1, 3, 5],
            list(video_ab.normalize(
                video_ab.quality_range(1, 5, 1),
                lambda x: x + (x % 2))))


class ReplaceWithMetavarsTest(unittest.TestCase):
    """Tests the code that replaces command args with metavars."""

    def testEmpty(self):
        self.assertEqual([], video_ab.replace_with_metavars([], {}))

    def testReplace(self):
        self.assertEqual(
            [['a', 'b', 'INPUT_FILENAME', 'OUTPUT_FILENAME', 'd'],
             ['d', 'OUTPUT_FILENAME']],
            video_ab.replace_with_metavars(
                [['a', 'b', 'if', 'of', 'd'], ['d', 'of']],
                {'if': 'INPUT_FILENAME',
                 'of': 'OUTPUT_FILENAME'}))

        
class VideoAbTest(unittest.TestCase):

    # Scratch directory for storing the test video.
    INPUT_DIR = None

    @classmethod
    def setUpClass(cls):
        cls.INPUT_DIR = tempfile.mkdtemp()
        print('\nGenerating test videos - this may take some time.')
        ffmpeg_test.gen_fading_video(FFMPEG_BIN, cls.fading_name())
        ffmpeg_test.gen_noise_video(FFMPEG_BIN, cls.noise_name())

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls.INPUT_DIR)

    @classmethod
    def fading_name(cls):
        return os.path.join(cls.INPUT_DIR, 'fading.mkv')

    @classmethod
    def noise_name(cls):
        return os.path.join(cls.INPUT_DIR, 'noise.mkv')

    def setUp(self):
        self.output_dir = tempfile.mkdtemp()
        
    def tearDown(self):
        shutil.rmtree(self.output_dir)

    def testUserFriendlyBitrate(self):
        self.assertEqual(
            '1.02e+08 bits/s',
            video_ab.user_friendly_bitrate(102358789))

    def testCalcBitrate(self):
        self.assertEqual(8 * os.path.getsize(self.noise_name()),
                         video_ab.calc_bitrate(FFMPEG_BIN, self.noise_name()))

        self.assertEqual((8 * os.path.getsize(self.fading_name())) / 10,
                         video_ab.calc_bitrate(FFMPEG_BIN, self.fading_name()))

    def runVideoAb(self, codec, mode, force, fname=None):
        """Runs the video_ab test on the codec, with qualities ranging from 0 to 1,
        in 0.2 increments."""
        fname = fname or self.noise_name()
        video_ab.run(FFMPEG_BIN, fname, self.output_dir,
                     codec, decimal.Decimal("0.0"), decimal.Decimal("1.0"),
                     decimal.Decimal("0.2"), mode, force)

    def testAllStreamsCopied(self):
        """Verify that if more than 1 audio and subtitle track are included, they
        are in the output files."""
        # The fading video has two audio and subtitle tracks.
        self.runVideoAb(video_ab.CODECS[0], video_ab.MODES['top'], False,
                        self.fading_name())
        self.verifyFilesGenerated()
        self.verifyFilesStreams([
            ffmpeg_test.VIDEO,
            ffmpeg_test.AUDIO, ffmpeg_test.AUDIO,
            ffmpeg_test.SUBTITLE, ffmpeg_test.SUBTITLE])

    def testForce(self):
        """Verify that if files exist and force is not set, an assertion is raised;
        otherwise, the files are overwritten; moreover, the modified times on the
        files should all update."""
        self.runVideoAb(video_ab.CODECS[0], video_ab.MODES['side-left'], False)
        self.verifyFilesGenerated()
        mtimes = [os.path.getmtime(os.path.join(self.output_dir, x)) for x in
                  os.listdir(self.output_dir)]
        self.assertRaises(RuntimeError, self.runVideoAb, video_ab.CODECS[1],
                          video_ab.MODES['side-left'], False)
        time.sleep(2)  # Ensure mtimes cannot be the same, even if transcoding is fast.
        self.runVideoAb(video_ab.CODECS[1], video_ab.MODES['side-left'], True)
        new_mtimes = [os.path.getmtime(os.path.join(self.output_dir, x)) for x in
                      os.listdir(self.output_dir)]
        self.assertTrue(min(new_mtimes) > max(mtimes))

    def verifyFilesStreams(self, expected_streams):
        """Verifies that all generated A/B videos have the provided streams."""
        videos = [video_ab.quality_basename(decimal.Decimal("0.0")),
                  video_ab.quality_basename(decimal.Decimal("0.2")),
                  video_ab.quality_basename(decimal.Decimal("0.4")),
                  video_ab.quality_basename(decimal.Decimal("0.6")),
                  video_ab.quality_basename(decimal.Decimal("0.8")),
                  video_ab.quality_basename(decimal.Decimal("1.0"))]
        for video in videos:
            fname = os.path.join(self.output_dir, video)
            streams = ffmpeg_test.read_streams(FFMPEG_BIN, fname)
            self.assertEqual(expected_streams, streams)
        
    def verifyFilesGenerated(self):
        """Verifies that a call to runVideoAb generates 7 non-empty files."""
        self.assertEqual(list(sorted(
            [video_ab.quality_basename(decimal.Decimal("0.0")),
             video_ab.quality_basename(decimal.Decimal("0.2")),
             video_ab.quality_basename(decimal.Decimal("0.4")),
             video_ab.quality_basename(decimal.Decimal("0.6")),
             video_ab.quality_basename(decimal.Decimal("0.8")),
             video_ab.quality_basename(decimal.Decimal("1.0")),
             video_ab.STATS_FILENAME])),
            list(sorted(os.listdir(self.output_dir))))
        for fname in os.listdir(self.output_dir):
            self.assertTrue(os.path.getsize(
                os.path.join(self.output_dir, fname)) > 0)

    def removeGeneratedFiles(self):
        """Remove all files in the output directory."""
        for fname in os.listdir(self.output_dir):
            os.remove(os.path.join(self.output_dir, fname))
        
    def testModes(self):
        for mode in video_ab.MODES:
            self.runVideoAb(video_ab.CODECS[0], video_ab.MODES[mode], False)
            self.verifyFilesGenerated()
            # Noise video has 1 video, audio, and subtitle track.
            self.verifyFilesStreams(
                [ffmpeg_test.VIDEO, ffmpeg_test.AUDIO, ffmpeg_test.SUBTITLE])
            self.removeGeneratedFiles()

    def testCodecs(self):
        for codec in video_ab.CODECS:
            self.runVideoAb(codec, video_ab.MODES['side-left'], False)
            self.verifyFilesGenerated()
            # Noise video has 1 video, audio, and subtitle track.
            self.verifyFilesStreams(
                [ffmpeg_test.VIDEO, ffmpeg_test.AUDIO, ffmpeg_test.SUBTITLE])
            self.removeGeneratedFiles()

    def testStats(self):
        """Verifies that the stats file has entries for all 6 encoding qualities."""
        self.runVideoAb(video_ab.CODECS[0], video_ab.MODES['side-left'], False)
        with open(os.path.join(self.output_dir, video_ab.STATS_FILENAME)) as f:
            text = f.read()
        qualities_bitrates = self.extract_qualities_bitrates(text)
        self.assertEqual([decimal.Decimal("0.0"),
                          decimal.Decimal("0.2"),
                          decimal.Decimal("0.4"),
                          decimal.Decimal("0.6"),
                          decimal.Decimal("0.8"),
                          decimal.Decimal("1.0")],
                         list(sorted(qualities_bitrates)))
        for bitrate in qualities_bitrates.values():
            self.assertTrue(bitrate > 0)

    def extract_qualities_bitrates(self, stats_text):
        r = {}
        for match in QUALITY_RE.finditer(stats_text):
            q = decimal.Decimal(match.groupdict()['quality'])
            self.assertFalse(q in r)
            r[q] = decimal.Decimal(match.groupdict()['bitrate'])
        return r


if __name__ == '__main__':
    unittest.main()
