#!/usr/bin/env python
#
# The MIT License (MIT)
#
# Copyright (c) 2016 Elliott Karpilovsky
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Extracts a slice from a video, to help with A/B testing of video codecs.

import argparse, os, random, re, subprocess, shlex, sys, time


# Keyword indicating a random offset should be calculated.
RANDOM = 'random'  
OFFSET_RE = re.compile('^\d\d:\d\d:\d\d$')

class FFMPEG(object):
    """This class contains functionality regarding FFMPEG commands. Normally,
    it would be a module in its own separate file. However, since I want this
    script to be stand-alone, it is represented here."""

    @staticmethod
    def read_length_sec(metadata):
        """Returns the length of the video, in seconds, from the FFMPEG metadata."""
        duration_re = re.compile(r'^  Duration: (\d\d):(\d\d):(\d\d)\.\d\d,', 
                                 re.MULTILINE)
        matches = duration_re.findall(metadata)
        if len(matches) != 1:
            raise ValueError('Expected single duration line. Matches: %s' % 
                             matches)
        match = matches[0]
        hours = int(match[0])
        minutes = int(match[1])
        seconds = int(match[2])
        return hours * 3600 + minutes * 60 + seconds

    @staticmethod
    def run(cmd):
        """Runs the command. Raises an exception if non-zero return status is 
        found. Otherwise returns (stdout, stderr)."""
        p = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
            stdin=subprocess.PIPE, close_fds=True)
        stdout, stderr = p.communicate()
        if p.returncode != 0:
            raise FFMPEG.CmdError(cmd, p.returncode, stdout, stderr)
        return stdout, stderr

    @staticmethod
    def read_metadata(ffmpeg_bin, fname):
        """Returns metadata about the file, returning the output from FFMPEG."""
        stderr = None
        cmd = [ffmpeg_bin, '-i', fname]
        try:
            stdout, stderr = FFMPEG.run(cmd)
            raise RuntimeError(
                "Reading metadata with FFMPEG should return "
                "status code of 1, but 0 was received. "
                "Information:\n{}".format(
                    FFMPEG.CmdError(cmd, p.returncode, stdout, stderr)))
        except FFMPEG.CmdError as e:
            if e.returncode != 1:
                raise RuntimeError(
                    "Reading file length with FFMPEG should return "
                    "status code of 1. Information:\n{}".format(str(e)))
            stderr = e.stderr
        return stderr.decode('utf-8')

    @staticmethod
    def logfile_prefix():
        """Returns a logfile prefix for FFMPEG that is unique on each invocation. 
        It is automatically deleted on program exit."""
        d = tempfile.mkdtemp(prefix='ffmpeg_')
        atexit.register(shutil.rmtree, d)
        return os.path.join(d, 'logfile')

    @staticmethod
    def ignore_output():
        """A destination for the output of ffmpeg that goes into the void."""
        if platform.system() == 'Windows':
            return 'NUL'
        elif platform.system() in ('Linux', 'Darwin'):
            return '/dev/null'
        else:
            raise RuntimeError(
                'Unsupported platform (must be Linux, MacOS X, or Windows): {}'.format(
                    platform.system()))

    @staticmethod
    def shell_cmd(cmd):
        """Convert a list of command/args into a single shell command."""
        return ' '.join(shlex.quote(x) for x in cmd)
    
    class CmdError(RuntimeError):
        """Error raised when a command fails to execute."""

        def __init__(self, cmd, returncode, stdout, stderr):
            readable_cmd = FFMPEG.shell_cmd(cmd)
            super(FFMPEG.CmdError, self).__init__(
                'Command: {}\n'
                'Return code: {}\n'
                'STDOUT: {}\n'
                'STDERR: {}\n'.format(readable_cmd, returncode,
                                      stdout.decode('utf-8'),
                                      stderr.decode('utf-8')))
            self.cmd = cmd
            self.returncode = returncode
            self.stdout = stdout
            self.stderr = stderr


class SecondsAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if values <= 0:
            parser.error("seconds must be positive")
        setattr(namespace, self.dest, values)

class OffsetAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if not (values == RANDOM or OFFSET_RE.match(values)):
            parser.error('offset must be expressed as HH:MM:SS (or "random")')
        setattr(namespace, self.dest, values)


PARSER = argparse.ArgumentParser(description="Slices a video.")
PARSER.add_argument('input', help='input file')
PARSER.add_argument('output', help='output file')
PARSER.add_argument('-b', '--ffmpeg-bin', metavar='/path/to/ffmpeg', default='ffmpeg',
                    help='location of the FFMPEG binary (default is "ffmpeg")')
PARSER.add_argument('-o', '--offset', metavar='HH:MM:SS|random', action=OffsetAction,
                    help='start at offset HH:MM:SS; may also use "random" to '
                    'have a random offset picked')
PARSER.add_argument('-s', '--seconds', metavar='N', action=SecondsAction, type=int,
                    help='only extract the first N seconds')
PARSER.add_argument('--ffmpeg_args', default='-map 0 -c copy', type=str,
                    help='additional arguments to pass to ffmpeg (default is '
                    '"-map 0 -c copy")')
PARSER.add_argument('-f', '--force', dest='force', action='store_true',
                    help='force overwriting of output file, if it exists')


def to_hhmmss(seconds):
    """Formats seconds as HH:MM:SS."""
    return time.strftime('%H:%M:%S', time.gmtime(seconds))


def to_sec(hhmmss):
    """Converts HH:MM:SS to seconds."""
    h, m, s = hhmmss.split(':')
    hours = int(h)
    minutes = int(m)
    seconds = int(s)
    return 60 * 60 * hours + 60 * minutes + seconds


def random_offset(length_sec, window_sec):
    """Picks a random offset that will run until the end of the window."""
    window_sec = window_sec or 1
    return random.randint(0, length_sec - window_sec)


def save_clip(ffmpeg_bin, ifname, ofname, offset,
              window_sec, ffmpeg_args, force):
    """Cuts out a clip of video from the input file, at the offset and for
    window_sec seconds, and saves it into the output file."""
    cmd = [
        ffmpeg_bin,
        '-i', ifname]
    if offset:
        cmd.extend(['-ss', offset])
    if window_sec:
        cmd.extend(['-t', to_hhmmss(window_sec)])
    if ffmpeg_args:
        cmd.extend(shlex.split(ffmpeg_args))
    if force:
        cmd.append('-y')
    cmd.append(ofname)
    FFMPEG.run(cmd)


def print_exit(s, return_code):
    """Prints the message to stderr, then exits with the return code."""
    print(s, file=sys.stderr)
    sys.exit(return_code)


def run(ffmpeg_bin, input_file, output_file, seconds, offset, ffmpeg_args, force):
    metadata = FFMPEG.read_metadata(ffmpeg_bin, input_file)
    length_sec = FFMPEG.read_length_sec(metadata)
    if length_sec < 1:
        raise ValueError('{} has less than 1 second of footage and cannot be '
                         'sliced'.format(input_file))
    if seconds is not None and seconds > length_sec:
        raise ValueError('{} has {} second(s) of footage, but a {} second window '
                         'was requested'.format(input_file, length_sec, seconds))
    if offset == RANDOM:
        offset = to_hhmmss(random_offset(length_sec, seconds))
    if offset is not None and to_sec(offset) >= length_sec:
        raise ValueError('{} has length {}, but requested offset is {}; the video '
                         'cannot be sliced'.format(
                             input_file, to_hhmmss(length_sec), offset))
    if offset is not None and seconds is not None and (
            to_sec(offset) + seconds > length_sec):
        raise ValueError('{} has length {}, but a slice at offset {} of length '
                         '{} was requested; the video is too short'.format(
                             input_file, to_hhmmss(length_sec), offset,
                             to_hhmmss(seconds)))
    save_clip(ffmpeg_bin, input_file, output_file, offset, seconds, ffmpeg_args,
              force)


if __name__ == '__main__':
    args = PARSER.parse_args()
    if not os.path.isfile(args.input):
        print_exit('input is not a file: {}'.format(args.input), os.EX_USAGE)
    if os.path.isfile(args.output) and not args.force:
        print_exit('--force not set and output file already exists: {}'.format(args.output),
                   os.EX_SOFTWARE)
    try:
        run(args.ffmpeg_bin, args.input, args.output, args.seconds, args.offset,
            args.ffmpeg_args, args.force)
    except Exception as e:
        print_exit(str(e), os.EX_SOFTWARE)
