Video A/B Tools
===============

A Python script that uses FFMPEG to generate multiple videos at different quality settings, to aid in A/B testing. A second Python script is included to easily slice a segment of a video for such a test.

![A screenshot of a video and its transcoded result, side by side](side-right.jpg)

Please see the [home page](https://ekarp.com/projects/video-ab/) for documentation.